class Frontier():
    def __init__(self):
        self.nodes = []
        self.visited = set()

    def add(self, node):
        self.nodes.append(node)
        self.visited.add(node)

    def pop(self, type):
        if type == 'dfs':
            return self.nodes.pop();
        elif type == 'bfs':
            return self.nodes.pop(0);

    def isVisited(self, node):
        return node in self.visited

    def empty(self):
        return not self.nodes

    def size(self):
        return len(self.nodes)