from frontier import Frontier
from node import Node

a = Node('a')
b = Node('b')
c = Node('c')
d = Node('d')
e = Node('e')
f = Node('f')

a.attach(b);
b.attach(a);
b.attach(c);
b.attach(d);
c.attach(e);
d.attach(f);

def solve(frontier, lookFor, path):
        if not frontier.nodes:
            print('no solution')
            return;

        node = frontier.pop('dfs')

        if node.value == lookFor:
            path.append(node.value)

            print(path)
            return

        for n in node.next:
            if not frontier.isVisited(n) and n.value != 'x':
                frontier.add(n)

        path.append(node.value)

        solve(frontier, lookFor, path)

lookFor = 'f'
frontier = Frontier()
frontier.add(a)
solve(frontier, lookFor, [])

