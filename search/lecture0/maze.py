from frontier import Frontier
from node import Node

def convertMazeToNodes(maze):
    height = len(maze);
    width = len(maze[0]);

    nodeMaze = []

    for x in range(height):
        row = []
        nodeMaze.append(row);
        for y in range (width):
            node = Node(maze[x][y], f'{x}, {y}', height - 1 - x + width - 1 - y)
            nodeMaze[x].append(node)

    for x in range(height):
        for y in range (width):
            if x - 1 >= 0:
                nodeMaze[x][y].attach(nodeMaze[x - 1][y])

            if y - 1 >= 0:
                nodeMaze[x][y].attach(nodeMaze[x][y - 1])

            if x + 1 <= height - 1:
                nodeMaze[x][y].attach(nodeMaze[x + 1][y])

            if y + 1 <= width - 1:
                nodeMaze[x][y].attach(nodeMaze[x][y + 1])

    return nodeMaze;

def solveMaze(frontier, path, steps):
    if frontier.empty():
        return

    node = frontier.pop('dfs')

    if node.value == 'f':
        path.append(node.location)
        print(path)
        return


    node.next.sort(key=lambda x: x.distance, reverse=True)

    for n in node.next:
        if not frontier.isVisited(n) and n.value != 'x':
            n.distance += steps
            frontier.add(n)

    path.append(node.location)

    steps += 1

    solveMaze(frontier, path, steps)


# Solution

x = 'x'
y = 'y'
s = 's'
f = 'f'

maze = [[s, y, y, y, y, y, y],
        [y, x, x, x, x, x, y],
        [y, x, y, y, y, y, y],
        [y, x, x, y, x, x, y],
        [y, y, y, y, y, y, f]]

frontier = Frontier()
nodeMaze = convertMazeToNodes(maze)
startNode = nodeMaze[0][0]
frontier.add(startNode)

solveMaze(frontier, [], 0)