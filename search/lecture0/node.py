class Node():
    def __init__(self, value, location = None, distance = None):
        self.value = value
        self.location = location
        self.distance = distance
        self.next = []

    def attach(self, node):
        self.next.append(node)

